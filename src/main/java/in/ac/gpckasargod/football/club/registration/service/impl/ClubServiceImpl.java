/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.football.club.registration.service.impl;


import in.ac.gpckasargod.football.club.registration.ui.model.data.Club;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasargod.football.club.registration.service.ClubService;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ctlab
 */
public class ClubServiceImpl extends ConnectionServiceImpl implements ClubService {

    @Override
    public String saveSports(String clubName, String clubLicense) {
        try {
            Connection connection = getConnection();
            Statement statement  = connection.createStatement();
            String query = "INSERT INTO CLUB_REGISTRATION (CLUB_NAME,CLUB_LICENSE) VALUES "+ "('"+clubName+"','"+clubLicense+"')";
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "save failed";
            }
            else{
                return "save successfull";
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClubServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
       
    }

    @Override
    public List<Club> getAllClub() {
        List<Club> clubs = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CLUB_REGISTRATION";
            ResultSet resultSet =statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String clubName = resultSet.getString("CLUB_NAME");
                String clubLicense = resultSet.getString("CLUB_LICENSE");
                Club club = new Club(id, clubName, clubLicense);
                clubs.add(club);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClubServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clubs;
    }

    @Override
    public String deleteClub(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM CLUB_REGISTRATION WHERE ID =?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "failed";
            else 
                return "delete successfull";
        } catch (SQLException ex) {
            Logger.getLogger(ClubServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "failed";
    }

   
    
}




     


        
            
            
        
    
    

