/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.football.club.registration.service;

import in.ac.gpckasargod.football.club.registration.ui.model.data.Club;
import java.util.List;

/**
 *
 * @author ctlab
 */
public interface ClubService {

   public String saveSports(String clubName,String clubLicense);
   public List<Club> getAllClub();
  public String deleteClub(Integer id);
}
