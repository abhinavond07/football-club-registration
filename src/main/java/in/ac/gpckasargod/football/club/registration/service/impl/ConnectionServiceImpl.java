/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.football.club.registration.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ctlab
 */
public class ConnectionServiceImpl {
    String jdbcUrl = "jdbc:mysql://localhost:3306/";
    String databaseName = "FOOTBALL_CLUB_REGISTRATION";
    String connectionString = jdbcUrl+databaseName;
    String username = "root";
    String password = "wishmeon7thmarch";
    public Connection getConnection() throws SQLException{
      Connection connection  = DriverManager.getConnection(connectionString,username,password); 
        return connection;
    }
    
}
