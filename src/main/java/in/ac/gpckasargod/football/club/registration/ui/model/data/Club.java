/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.football.club.registration.ui.model.data;

/**
 *
 * @author ctlab
 */
public class Club {
    private Integer id;
    private String clubName;
    private String clubLicense;

    public Club(Integer id, String clubName, String clubLicense) {
        this.id = id;
        this.clubName = clubName;
        this.clubLicense = clubLicense;
    }

    public Integer getId() {
        return id;
    }

    public String getClubName() {
        return clubName;
    }

    public String getClubLicense() {
        return clubLicense;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public void setClubLicense(String clubLicense) {
        this.clubLicense = clubLicense;
    }

    
}
    